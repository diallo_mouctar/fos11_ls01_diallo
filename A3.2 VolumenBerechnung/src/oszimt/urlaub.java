package oszimt;
import java.util.Scanner;
public class urlaub {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Gesamt Reisebudget eingeben in:" + "EURO");
		double euro = scan.nextDouble();
		System.out.println("Geben Sie der Betrag in Euro, der Sie in Dollar Wechsel wollen:" + "Euro");
		double dollar = scan.nextDouble();
		System.out.println("Geben Sie der Betrag in Euro, die Sie in Yen wechsel wollen:" + "Euro");
		double yen = scan.nextDouble();
		System.out.println("Geben Sie der betrag in Euro, der Sie in Pfund wechsel wollen:" + "Euro");
		double pfund = scan.nextDouble();
		System.out.println("Geben Sie der Betrag in Euro, der Sie in Schweizer Frank wechsel wollen:" + "Euro");
		double schweizerF = scan.nextDouble();
		System.out.println("Geben Sie der Betrag in Euro, der Sie in Schwedische wechsel wollen:" + "Euro");
		double schwedischeK = scan.nextDouble();

		double erg1 = rechnenDollar(euro,dollar);
		System.out.println("Wechselte Geld in USA:" + erg1 + "USD");
		double erg2 = rechnenYen(euro,yen);
		System.out.println("Wechselte Geld in Japan:" + erg2 + "JPY");
		double erg3 = rechnenPfund(euro,pfund);
		System.out.println("Wechselte Geld in England:" + erg3 + "GBP");
		double erg4 = rechnenSf(euro,schweizerF);
		System.out.println("Wechselt Geld in der Schweiz:" + erg4 + "CHF");
		double erg5 = rechnenSk(euro,schwedischeK);
		System.out.println("Wechselte Geld in Schweden:" + erg5 + "SEK");
		scan.close();
	}
	public static double rechnenDollar(double euro, double dollar) {
		return euro * dollar;
	}

	public static double rechnenYen(double euro, double yen) {
		return euro * yen;
	}	
	public static double rechnenPfund(double euro, double pfund) {
		return euro * pfund;
	}
	public static double rechnenSf(double euro, double sf) {
		return euro * sf;
	}
	public static double rechnenSk(double euro, double sk) {
		return euro * sk;
	}

}
