package oszimt;

public class A3ModuloB {

	public static void main(String[] args) {
		int n = 200;

		for(int i = 1; i <= n; i++) {
			if((i % 4 == 0) && (i % 5 != 0))
				System.out.println(i + ": Teilbar ");
			else
				System.out.println(i + ": nicht Teilbar ");

		}

	}
}
