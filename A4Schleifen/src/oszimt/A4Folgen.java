package oszimt;

public class A4Folgen {

	public static void main(String[] args) {

		System.out.print("a) ");
		for (int i = 99; i >= 9; i = i - 3) {
			System.out.print(i + ", ");

		}
		System.out.println();
		System.out.print("b) ");
		for (int i = 1; i <= 20; i++) {
			System.out.print(i * i + ", ");
		}
		System.out.println();
		System.out.print("c) ");
		for (int i = 2; i <= 102; i = i + 4) {
			System.out.print(i + ", ");
		}
		System.out.println();
		System.out.print("d) ");
		for (int i = 2; i * i <= 1024; i = i + 2) {
			System.out.print(i * i + ", ");
		}
		System.out.println();
		System.out.print("e) ");
		for (int i = 2; i <= 32768; i = i * 2) {
			System.out.print(i + ",");
		}
	}
}
