package oszimt;

import java.util.Scanner;

public class A2SummeC {

	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.println("Bitte N eingeben :");
		int n = scan.nextInt();
		for(int i = 1; i <= (2 * n + 1); i = i + 2) {
			if(i < (2 * n + 1))
				System.out.print(i + ", ");
			else
				System.out.println(i );	
		}

	}
}
