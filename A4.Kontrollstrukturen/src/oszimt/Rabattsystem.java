package oszimt;
import java.util.Scanner;
public class Rabattsystem {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.println("Bestelltwert eingeben");
		double betrag = in.nextDouble();
		int bestelltwert = 100;
		int rabat2 = 500;
		if(betrag <= bestelltwert) {
			System.out.println("Der kunde bekommt ein Rabatt von 10%: " + betrag + " : " + (betrag * 0.1 ));
		}
		else if(betrag <= rabat2) {
			System.out.println("Der Kunde bekommt ein Rabatt von 15%: " + betrag + " : " + (betrag * 0.15));
		}
		else {
			System.out.println("Der Kunde bekommt ein Rabatt von 20%: " + betrag + " : " + (betrag * 0.2));
		}
		in.close();
	}

}
