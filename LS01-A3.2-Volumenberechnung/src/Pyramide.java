 import java.util.Scanner;

public class Pyramide {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie die Fl�che des Pyramides ein:");
		double a = scan.nextDouble();
		System.out.println("Geben Sie die H�her des Pyramides ein:");
		double h = scan.nextDouble();

		double erg = berechnePyramidevolumen(a,h);
		System.out.println("Volum von Pyramide ist:" + erg);
	}

	public static double berechnePyramidevolumen(double seitefl�che, double h�her) {
		double ergebnis = (seitefl�che * seitefl�che * h�her)/3;
		return ergebnis;
	}
}
